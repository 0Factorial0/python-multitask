def multitask():
    import sys
    import os

    test_os = os.name

    #check the os type, nt = windows, posix = unix
    #create a thread and wait for 1 second
    #then return if the key is pushed or not
    if test_os == "nt":

        import sys, time, msvcrt

        timeout = 1
        startTime = time.time()
        inp = None

        print ("Press Enter Key Or Wait...")
        while True:
            if msvcrt.kbhit():
                inp = msvcrt.getch()
                break
            elif time.time() - startTime > timeout:
                break
            time.sleep(0.5)

        if inp:
            return 1
        else:
            return 0
    #create a thread and wait for 1 second
    #then return if the key is pushed or not
    elif test_os == "posix":

        from select import select

        print ("Press Enter Key Or Wait...")
        timeout = 1
        rlist, wlist, xlist = select([sys.stdin], [], [], timeout)

        if rlist:
            return 1
        else:
            return 0
    else:
        return 1
